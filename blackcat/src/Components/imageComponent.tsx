import React from 'react';
import '../css/LoginContainer.scss'

const ImageComponent = (props: any) => {
  return (
    <img src={props.src} style={props.logoCat || props.stylingIcon} alt=""/>
  )

}

export default ImageComponent