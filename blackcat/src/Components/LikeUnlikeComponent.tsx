import React from 'react';
import '../css/HomeContainer.scss'
import '../App.css'
import '../css/ActivityList.scss'

const LikeUnlikeComponent = (props: any) => {
    let userId = props.data.some((data: any) => {
      if(data.userId == localStorage.getItem('userId')) {
        return true
      } else {
        return false
      }
    })

    if(userId) {
      return (
        <div className="likes">
          <div className="likes-logo" onClick={props.click}>
            <img src={props.imgLogo} alt="like" style={{height: '12px', width: '12px'}}></img>
          </div>
          <div className="likes-count">
            {props.onLike}
          </div>
        </div>
      )
    } else {
      return (
        <div className="likes">
          <div className="likes-logo" onClick={props.click}>
            <img src={props.outlineImgLogo} alt="like" style={{height: '12px', width: '12px'}}></img>
          </div>
          <div className="likes-count">
            {props.count} {props.word}
          </div>
        </div>
      )
    }
}

export default LikeUnlikeComponent