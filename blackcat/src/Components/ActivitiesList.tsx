import React, { useState } from 'react';
import '../css/HomeContainer.scss'
import '../App.css'
import '../css/ActivityList.scss'
import LikeUnlikeComponent from './LikeUnlikeComponent'
import axios from 'axios'

const ActivitiesList = (props : any) => {
  const [likesData, setLikesData] = useState(props.data.likes)
  const [likesCount, setLikesCount] = useState(props.data.likes.length)
  const [goingData, setGoingData] = useState(props.data.going)
  const [goingCount, setGoingCount] = useState(props.data.going.length)

  const likes = (e: any) => {
    e.preventDefault()
    let userId = localStorage.getItem('userId')
    axios.post(`http://localhost:3000/activities/${props.data.id}/likes/${userId}`)
      .then((data) => {
        setLikesData(data.data)
        setLikesCount(data.data.length)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const going = (e:any) => {
    e.preventDefault()
    let userId = localStorage.getItem('userId')
    axios.post(`http://localhost:3000/activities/${props.data.id}/going/${userId}`)
      .then((data) => {
        setGoingData(data.data)
        setGoingCount(data.data.length)
      })
      .catch((err) => {
        console.log(err)
      })
  }

  return(
    <div className="activity-list">
        <div className="activity-header">
          <div className="usernameAndLogo">
            <div className="logo">
              <img src="/user.svg" alt="username" style={{height: '20px', width: '20px'}}/>
            </div>
            <div className="username">
              {props.data.users.username}
            </div>
          </div>
          <div className="channelName">
            {props.data.channelName}
          </div>
        </div>

      <div className="activity-body">
        <div className="activity-title">
          {props.data.title}
        </div>
        <div className="activity-date">
          <div className="date-logo">
            <img src="/time.svg" alt="clock" style={{height: '12px', width: '12px'}}/>
          </div>
          <div className="event-date">
            {props.data.date}
          </div>
        </div>
        <div className="activity-content">
          {props.data.content}
        </div>
      </div>

      <div className="activity-footer">
        <LikeUnlikeComponent 
          data={likesData}
          count={likesCount}
          imgLogo='/like.svg'
          outlineImgLogo='/like-outline.svg'
          click={likes}
          onLike='I like it'
          word='Likes'
        />
        <LikeUnlikeComponent
          data={goingData}
          count={goingCount}
          imgLogo='/check.svg'
          outlineImgLogo='/check-outline.svg'
          click={going}
          onLike='I am going!'
          word='Going'
        />
      </div>

      <div className="line-border"> 
      </div>
    </div>
  )
}

export default ActivitiesList