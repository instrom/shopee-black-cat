import React from 'react';
import '../css/HomeContainer.scss'
import '../App.css'

export default function navigationBar() {
    return (
      <div className="navBar-container">
        <div className="navBar">
          <div className="searchIcon">
            <img src="/search.svg" alt="search"></img>
          </div>
          <div className="catIcon">
            <img src="/logo-cat.svg" alt="cat"></img>
          </div>
          <div className="giraffeIcon">
            <img src="/past.svg" alt="giraffe"/>
          </div>
        </div>
      </div>
    )
}