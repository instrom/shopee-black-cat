import axios from 'axios';

export const getActivities = () => {
  return (dispatch: any) => {
    axios.get('http://localhost:3000/activities')
      .then((res) => {
        dispatch({
          type: 'FETCH_ALL_DATA_SUCCESS',
          payload: res.data
        })
      })
      .catch((err) => {
        dispatch({
          type: 'FETCH_ALL_DATA_ERROR'
        })
        console.log(err)
      })
  }
}
