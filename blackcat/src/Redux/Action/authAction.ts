import axios from 'axios';

export const Login = (username: any, password: any) => {
  return (dispatch:any) => {
      axios.get(`http://localhost:3000/users?username=${username}&password=${password}`)
        .then((res) => {
          if(res.data.length > 0) {
            localStorage.setItem('token', 'token asal 23098240234')
            localStorage.setItem('userId', res.data[0].id)
            localStorage.setItem('username', res.data[0].username)
            dispatch({
              type: 'LOGIN',
              payload: res.data[0]
            })
          } else {
            dispatch({
              type: 'WRONG_USERNAME_PASSWORD'
            })
          }
        })
        .catch((err) => {
          console.log(err)
          dispatch({
            type: 'LOGOUT'
          })
        })
  }
}

