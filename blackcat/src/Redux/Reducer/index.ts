import { combineReducers } from 'redux';
import { authReducer } from './authReducer';
import { activitiesReducer } from './activitiesReducer'


// export default combineReducers(
//   {auth: authReducer},
//   {activity: activitiesReducer}
// )

const rootReducer = combineReducers({
  auth: authReducer,
  activity: activitiesReducer
})

export default rootReducer