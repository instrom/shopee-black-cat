const INITIAL_STATE = {
  loading:true,
  activities: [],
  errorMessage: ''
}

export const activitiesReducer = (state = INITIAL_STATE, action: any) => {
  switch(action.type) {
    case `FETCH_ALL_DATA_LOADING`:
      return {
        ...state,
        loading: true,
        errorMessage: null
      }
    case `FETCH_ALL_DATA_SUCCESS`: 
      return {
        ...state,
        loading:false,
        activities: action.payload,
        errorMessage: null
      }
    case `FETCH_ALL_DATA_ERROR`: 
      return {
        ...state,
        loading: false,
        errorMessage: 'error bang',
        activities: []
      }
    default:
      return state
  }
}


