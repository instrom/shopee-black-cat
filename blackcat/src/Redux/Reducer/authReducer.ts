const INITIAL_STATE = {
    username: '',
    password: '',
    logged: false,
    errorMessage: ''
}

export const authReducer = (state = INITIAL_STATE, action: any) => {
  switch(action.type) {
    case `LOGIN`:
      console.log(action)
      return {
        logged: true,
        errorMessage: ''
      }
    case `WRONG_USERNAME_PASSWORD`: {
      return {
        logged: false,
        errorMessage: 'Wrong username/password!'
      }
    }
    default:
      return state
  }
}