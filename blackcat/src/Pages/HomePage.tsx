import React, { useEffect } from 'react';
import { connect } from 'react-redux'; 
import { withRouter, Redirect } from 'react-router-dom';
import '../css/HomeContainer.scss';
import '../App.css';
import NavBar from '../Components/navigationBar';
import { getActivities } from '../Redux/Action/activitiesAction';
import ActivitiesList from '../Components/ActivitiesList';



const HomePage = (props: any) => {

  useEffect(() => {
    props.getActivities()
  }, [])
  if(localStorage.getItem('token')) {
    if(props.loading) {
      return (
        <div>
          <NavBar></NavBar>
          <h1>
            Loading...
          </h1>
        </div>
      )
    } else {
      return (
        <div>
          <NavBar></NavBar>
          {props.data.map((i : any) => {
            return(
              <ActivitiesList key={i.id} data={i}></ActivitiesList>
            )
          })}
        </div>
      )
    }
  } else {
    return(
      <Redirect to="/"></Redirect>
    )
  }
}

const mapStateToProps = (state : any) => {
  return {
    loading: state.activity.loading,
    data: state.activity.activities,
    errorMessage: state.activity.errorMessage
  }
}


export default withRouter(connect(mapStateToProps, { getActivities })(HomePage))