import React, { useState } from 'react';
import { connect } from 'react-redux'; 
import { withRouter, Redirect } from 'react-router-dom';
import { Login } from '../Redux/Action/authAction';
import '../css/LoginContainer.scss';
import ImageComponent from '../Components/ImageComponent';

const LoginPage = (props: any) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  let loginForm = (e:any) => {
    e.preventDefault()
    props.Login(username, password)
  }
  if (props.logged || localStorage.getItem('token')) {
    return (
      <Redirect to="/home" />
    )
  }

  const catLogo = {
    height: '64px',
    width: '64px',
    borderRadius: '50px',
    border: '#D5EF7F solid 2px',
    padding: '5px'
  }

  const iconUserPassword = {
    height: '13px',
    width: '13px'
  }

  return (
    <div className="LoginContainer">
      <div className="first-title">
       <span>
         FIND THE MOST LOVED ACTIVITIES
       </span>
      </div>
      <div className="second-title">
       <span>
         BLACK CAT
       </span>
      </div>
      <div className="cat-image">
        <ImageComponent src="/logo-cat.svg" logoCat={catLogo}></ImageComponent>
      </div>
      <div className="login-form-container">
        <form onSubmit={loginForm} className="loginForm">
          <div className="input-container">
            <div className="icon-user-password">
              <ImageComponent  src="/user.svg" stylingIcon={iconUserPassword}></ImageComponent>
            </div>
            <input type="text" onChange={e => setUsername(e.target.value)} placeholder="Username"></input>
          </div>
          <div className="input-container" style={{marginTop: '24px'}}>
            <div className="icon-user-password">

              <ImageComponent  src="/password.svg" stylingIcon={iconUserPassword}></ImageComponent>
            </div>
            <input type="password" onChange={e => setPassword(e.target.value)} placeholder="Password"></input>
          </div>
          <div className="buttonSignIn">
            <input type="submit" value="SIGN IN"></input>
          </div>
          <h4>{props.errorMessage}</h4>
        </form>
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => {
  return {
    logged: state.auth.logged,
    errorMessage: state.auth.errorMessage
  }
}




export default withRouter(connect(mapStateToProps, {Login})(LoginPage))