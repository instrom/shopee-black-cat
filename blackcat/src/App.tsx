import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Route, Link, Switch, Redirect} from 'react-router-dom';
import LoginPage from './Pages/LoginPage'
// import HomePage from './Pages/HomePage.tsx'
import HomePage from './Pages/HomePage'

function App() {
  function loggedIn() {
    if(localStorage.getItem('token')) {
      return true
    } else {
      return false
    }
  }
  return (
    <div className="App">
      <Switch>
        <Route exact path="/">
          <LoginPage/>
        </Route>
        <Route path="/home">
           <HomePage></HomePage>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
