const jsonServer = require('json-server')
const low = require('lowdb')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults()
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares)

// Add custom routes before JSON Server router
server.get('/activities/:id/likes', (req, res) => {
  let id = Number(req.params.id)
  let activitiesLikes = db.get('activities')
                          .find({id: id})
                          .get('likes')
  if(activitiesLikes) {
      res.jsonp(activitiesLikes)
  } else {
      res.sendStatus(500)
  }
})

server.post('/activities/:id/likes/:userId', (req,res) => {
  let id = Number(req.params.id)
  let userId = Number(req.params.userId)
  let activitiesLikes = db.get('activities')
                          .find({id: id})
                          .get('likes')
                          .find({userId: userId})
  if(activitiesLikes.value()) {
    db.get('activities')
      .find({id: id})
      .get('likes')
      .remove({userId: userId})
      .write()
    
    let allActivitiesLikes = db.get('activities')
                               .find({id: id})
                               .get('likes')
    res.jsonp(allActivitiesLikes)
    // res.sendStatus(200)
  } else {
    // console.log('masuk sini?')
    db.get('activities')
      .find({id: id})
      .get('likes')
      .push({userId: userId})
      .write()
    let allActivitiesLikes = db.get('activities')
                               .find({id: id})
                               .get('likes')
    res.jsonp(allActivitiesLikes)
    // res.sendStatus(201)
  }
})

server.post('/activities/:id/going/:userId', (req,res) => {
  let id = Number(req.params.id)
  let userId = Number(req.params.userId)
  let activitiesGoing = db.get('activities')
                          .find({id: id})
                          .get('going')
                          .find({userId: userId})
   if(activitiesGoing.value()) {
    db.get('activities')
      .find({id: id})
      .get('going')
      .remove({userId: userId})
      .write()
    
    let allActivitiesGoing = db.get('activities')
                               .find({id: id})
                               .get('going')
    res.jsonp(allActivitiesGoing)
  } else {
    db.get('activities')
      .find({id: id})
      .get('going')
      .push({userId: userId})
      .write()
    let allActivitiesGoing = db.get('activities')
                               .find({id: id})
                               .get('going')
    res.jsonp(allActivitiesGoing)
  }
})

server.get("/echo", (req, res) => {
    console.log('heya')
    res.jsonp(req.query);
});

server.get('/halo', (req, res) => {
    console.log('test')
    res.send(200)
})

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser)
server.use((req, res, next) => {
  if (req.method === 'POST') {
    req.body.createdAt = Date.now()
  }
  // Continue to JSON Server router
  next()
})

// Use default router
server.use(router)
server.listen(3000, () => {
  console.log('JSON Server is running')
})